﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Fireworks
{
    public static class SidesCalculator
    {
        public static List<List<Vector2Int>> CountsSides(Mesh mesh)
        {
            var tria = mesh.triangles;
            var sides = CountSides(tria);
            var list = sides.ToList();
            return SortSides(list);
        }

        private static HashSet<Vector2Int> CountSides(int[] tria)
        {
            var sides = new HashSet<Vector2Int>();
            int n1, n2, n3;

            for (int i = 0; i < tria.Length; i += 3)
            {
                n1 = tria[i];
                n2 = tria[i + 1];
                n3 = tria[i + 2];
                AddEdge(sides, n1, n2);
                AddEdge(sides, n2, n3);
                AddEdge(sides, n3, n1);
            }

            return sides;
        }

        private static List<List<Vector2Int>> SortSides(List<Vector2Int> list)
        {
            var sortedSides = new List<List<Vector2Int>>(10);
            int n = 0;
            bool indexFounded = false;
            while (list.Count > 0)
            {
                if (indexFounded)
                    sortedSides.Last().Add(list[n]);
                else
                    sortedSides.Add(new List<Vector2Int>(list.Count) {list[n]});

                indexFounded = false;
                int nextPoint = list[n].y;
                list.RemoveAt(n);
                n = 0;
                UnityEngine.Profiling.Profiler.BeginSample("For");

                for (var i = 0; i < list.Count; i++)
                    if (list[i].x == nextPoint)
                    {
                        n = i;
                        indexFounded = true;
                        break;
                    }

                UnityEngine.Profiling.Profiler.EndSample();
            }

            return sortedSides;
        }

        private static void AddEdge(HashSet<Vector2Int> list, int vx, int vy)
        {
            if (!list.Remove(new Vector2Int(vy, vx)))
                list.Add(new Vector2Int(vx, vy));
        }
    }
}
