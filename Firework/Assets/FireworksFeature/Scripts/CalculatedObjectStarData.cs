﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;
using static Fireworks.BWMath;
using static Fireworks.TriangleMath;

namespace Fireworks
{
    public class CharacterStarData
    {
        public GroupStarData[] groups;

        public CharacterStarData(int count)
        {
            groups = new GroupStarData[count];
        }
    }

    public class GroupStarData
    {
        public ObjectStarData[] objects;
        public Vector3 center;

        public GroupStarData(int count, Vector3 center)
        {
            objects = new ObjectStarData[count];
            this.center = center;
        }
    }

    public class BonedObjectStarData : ObjectStarData
    {
        public List<StarBW> weights;
        public Matrix4x4[] bindPoses;
        public Transform[] bones;

        private Matrix4x4[] frameMatrix;

        public override Vector3[] GetCurrentPoints()
        {
            Profiler.BeginSample("VertexCalculation");

            CheckArrays();

            for (int i = 0; i < bones.Length; i++)
                frameMatrix[i] = bones[i].localToWorldMatrix * bindPoses[i];

            for (int i = 0; i < poses.Length; i++)
                poses[i] = CalculatePos(weights[i], points[i], frameMatrix);

            Profiler.EndSample();
            return poses;
        }

        public BonedObjectStarData(Matrix4x4[] bindPoses, Transform[] bones, int starIndex, int lineWidth = -1)
        {
            this.bindPoses = bindPoses;
            this.bones = bones;
            this.starIndex = starIndex;
            this.lineWidth = lineWidth;
            points = new List<Vector3>();
            weights = new List<StarBW>();
        }

        private void CheckArrays()
        {
            if (poses == null || poses.Length != points.Count)
                poses = new Vector3[points.Count];

            if (frameMatrix == null || frameMatrix.Length != bones.Length)
                frameMatrix = new Matrix4x4[bones.Length];
        }
    }

    public class NotBonedObjectStarData : ObjectStarData
    {
        public Transform parent;
        public Matrix4x4 bindPose;

        private Matrix4x4 frameMatrix;

        public NotBonedObjectStarData(Transform parent, Matrix4x4 bindPose, int starIndex, int lineWidth = -1)
        {
            this.parent = parent;
            this.bindPose = bindPose;
            this.starIndex = starIndex;
            this.lineWidth = lineWidth;
            points = new List<Vector3>();
        }

        public override Vector3[] GetCurrentPoints()
        {
            Profiler.BeginSample("VertexCalculation");

            if (poses == null || poses.Length != points.Count)
                poses = new Vector3[points.Count];

            frameMatrix = parent.localToWorldMatrix * bindPose;

            for (int i = 0; i < poses.Length; i++)
                poses[i] = frameMatrix.MultiplyPoint(points[i]);

            Profiler.EndSample();

            return poses;
        }
    }

    public abstract class ObjectStarData
    {
        public List<Vector3> points;

        public int starIndex;
        public int lineWidth;

        public HidePolygonData[] hidePolygons;
        internal Vector3[] poses;
        private Vector2[][] Polygon;
        public abstract Vector3[] GetCurrentPoints();

        public Vector2[][] GetCalculatedPolygons()
        {
            CalculatePolygon(ref Polygon, hidePolygons);
            return Polygon;
        }
    }
}
