﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Profiling;
using static Fireworks.TriangleMath;
using static Fireworks.PointHider;
using static Fireworks.StarDataCalculator;

namespace Fireworks
{
    public class CharacterDataAnimator : MonoBehaviour
    {
        public bool spawnObjects;
        public float starStep;

        public GroupData[] groups;

        public float timeScale;

        public CharacterStarData CalculatedData;

        private Transform starParent;
        private List<Transform> stars;
        private List<MeshRenderer> starsRend;

        private Animation _animation;
        private List<Renderer> _baseRenderers;

        public void Awake()
        {
            _animation = GetComponent<Animation>();
            _baseRenderers = GetComponentsInChildren<Renderer>().ToList();
            
            CalculatedData = CountCharacterData(groups, starStep);

            if (!spawnObjects) return;

            starParent = new GameObject("starsParent").transform;
            stars = new List<Transform>();
            starsRend = new List<MeshRenderer>();
            foreach (var group in CalculatedData.groups)
            foreach (var groupObject in group.objects)
            foreach (var pos in groupObject.GetCurrentPoints())
            {
                var spawnObject = SpawnObject(pos, starParent);
                stars.Add(spawnObject);
                starsRend.Add(spawnObject.GetComponent<MeshRenderer>());
            }
        }

        private void Update()
        {
            if (!spawnObjects) return;

            Time.timeScale = timeScale;

            Profiler.BeginSample("SetAll");

            UpdateByPoly();

            Profiler.EndSample();
        }

        public void UpdateByPoly()
        {
            var i = 0;
            foreach (var group in CalculatedData.groups)
            foreach (var groupObject in group.objects)
            {
                var poly = groupObject.GetCalculatedPolygons();

                foreach (var pos in groupObject.GetCurrentPoints())
                {
                    starsRend[i].enabled = !PointHided(pos, poly);
                    stars[i++].position = pos;
                }
            }
        }

        private void OnDrawGizmosSelected()
        {
            Color[] color = {Color.red, Color.blue, Color.green, Color.yellow, Color.cyan, Color.magenta};
            int k = 0;
            foreach (var group in groups)
            foreach (var groupObject in group.objects)
            {
                if (groupObject.hidePolygons == null || groupObject.hidePolygons.Length == 0) continue;
                Gizmos.color = color[k++ % color.Length];

                Vector2[][] poly = null;
                CalculatePolygon(ref poly, groupObject.hidePolygons);
                if (poly == null) continue;

                foreach (var polygon in poly)
                    for (int i = 0; i < polygon.Length; i++)
                        Gizmos.DrawLine(polygon[i], polygon[(i + 1) % polygon.Length]);
            }
        }

        private Transform SpawnObject(Vector3 pos, Transform parent)
        {
            var sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.transform.localScale = Vector3.one * 15;
            sphere.transform.position = pos;
            sphere.transform.parent = parent;
            return sphere.transform;
        }

        public void OnGUI()
        {
            var isPlaying = _animation.isPlaying;
            if (GUI.Button(new Rect(0, 0, Screen.width / 10, Screen.height / 10), isPlaying ? "StopAnimation" : "StartAnimation"))
            {
                SetRenderersEnabled(isPlaying);

                if (isPlaying)
                {
                    _animation.Stop();
                }
                else
                {
                    _animation.Play();
                }
            }
        }

        //Shaders might be not exactly the same version vs assets, but it haven't needed them for feature testing, so it's more or less represent what's going on
        private void SetRenderersEnabled(bool isEnabled)
        {
            _baseRenderers.ForEach(x => x.enabled = isEnabled);
        }
    }
}
