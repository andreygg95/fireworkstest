﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Fireworks
{
    public class StarDataCalculator
    {
        public static CharacterStarData CountCharacterData(GroupData[] groups, float starStep)
        {
            var allObjectsOrder = CountObjectsOrder(groups);

            var characterData = new CharacterStarData(groups.Length);

            for (var i = 0; i < groups.Length; i++)
                characterData.groups[i] = CountGroupStarData(groups[i], allObjectsOrder, starStep);

            return characterData;
        }

        private static List<Transform> CountObjectsOrder(GroupData[] groups)
        {
            var allObjectsOrder = new List<Transform>();

            foreach (var data in groups)
                allObjectsOrder.AddRange(data.objects.Select(x => x.transform));

            Debug.Log("allObjectsOrder: " + allObjectsOrder.Count);
            allObjectsOrder = allObjectsOrder.OrderBy(x => x.position.z).ToList();

            return allObjectsOrder;
        }

        private static GroupStarData CountGroupStarData(GroupData data, List<Transform> allObjectsOrder, float starStep)
        {
            var center = GetCenter(data);
            var pd = new GroupStarData(data.objects.Length, center);

            for (var i = 0; i < data.objects.Length; i++)
            {
                var transform = data.objects[i].transform;
                var skmr = transform.GetComponent<SkinnedMeshRenderer>();
                if (skmr != null)
                    pd.objects[i] = CountStarsBoned(data.objects[i], allObjectsOrder, starStep);
                else
                    pd.objects[i] = CountStarsNotBoned(data.objects[i], allObjectsOrder, starStep);
            }

            return pd;
        }

        private static BonedObjectStarData CountStarsBoned(StarObject star, List<Transform> allObjectsOrder,
            float starStep)
        {
            var preData = CalculateSkinnedPoints(star.transform);

            float z = allObjectsOrder.IndexOf(star.transform);
            var data = new BonedObjectStarData(preData.bindposes, preData.bones, star.starIndex, star.lineWidth);

            InitDataTriangles(data, star.hidePolygons);

            var indices = star.usedMeshParts == "" ? null : star.usedMeshParts.Split(',', '.').ToList();
            for (var index = 0; index < preData.points.Count; index++)
            {
                if (!(indices == null || indices.Contains(index.ToString())))
                    continue;
                var count = preData.points[index].Count;
                var currentLength = starStep;
                for (int i = 0; i < count; i++)
                    CountPoints(data, preData, index, i, (i + 1) % count, z, starStep, ref currentLength);
            }

            return data;
        }

        private static NotBonedObjectStarData CountStarsNotBoned(StarObject star, List<Transform> allObjectsOrder,
            float starStep)
        {
            var preData = CalculateNonSkinnedPoints(star.transform);

            float z = allObjectsOrder.IndexOf(star.transform);
            var data = new NotBonedObjectStarData(preData.parent, preData.bindpose, star.starIndex, star.lineWidth);
            InitDataTriangles(data, star.hidePolygons);

            var indices = star.usedMeshParts == "" ? null : star.usedMeshParts.Split(',', '.').ToList();
            for (var index = 0; index < preData.points.Count; index++)
            {
                if (!(indices == null || indices.Contains(index.ToString())))
                    continue;
                var points = preData.points[index];
                var currentLength = starStep;
                for (int i = 0; i < points.Count; i++)
                    CountPoints(data, points[i], points[(i + 1) % points.Count], z, starStep, ref currentLength);
            }

            return data;
        }

        private static void InitDataTriangles(ObjectStarData data, HidePolygonData[] polygons)
        {
            data.hidePolygons = polygons;
            foreach (var polygon in polygons)
            {
                if (polygon.transform == null) continue;

                var skm = polygon.transform.GetComponent<SkinnedMeshRenderer>();

                if (skm != null)
                {
                    var mesh = skm.sharedMesh;
                    polygon.SetBones(skm.bones, mesh.bindposes);
                    polygon.CalculateWeights(mesh.vertices, mesh.boneWeights);
                }
            }
        }

        private static void CountPoints(BonedObjectStarData data, BonedPreCalculatedData preData, int listIndex, int i1,
            int i2, float z, float starStep, ref float currentLength)
        {
            var prev = preData.points[listIndex][i1];
            var next = preData.points[listIndex][i2];
            var delta = next - prev;
            var length = delta.magnitude;
            var direction = delta / length;

            if (length + currentLength > starStep)
            {
                var b1 = preData.weights[listIndex][i1];
                var b2 = preData.weights[listIndex][i1];
                float f = 0;

                while (length + currentLength > starStep)
                {
                    prev += direction * (starStep - currentLength);
                    f += (starStep - currentLength) / length;
                    length -= starStep - currentLength;

                    data.points.Add(new Vector3(prev.x, prev.y, z));
                    data.weights.Add(BWInterpolator.InterpolateBW(b1, b2, f));

                    currentLength = 0;
                }
            }

            currentLength += length;
        }

        private static void CountPoints(NotBonedObjectStarData data, Vector2 prev, Vector2 next, float z,
            float starStep, ref float currentLength)
        {
            var delta = next - prev;
            var length = delta.magnitude;
            var direction = delta / length;
            while (length + currentLength > starStep)
            {
                prev += direction * (starStep - currentLength);
                length -= starStep - currentLength;
                data.points.Add(new Vector3(prev.x, prev.y, z));
                currentLength = 0;
            }

            currentLength += length;
        }

        private static NotBonedPreCalculatedData CalculateNonSkinnedPoints(Transform t)
        {
            var mesh = t.GetComponent<MeshFilter>().sharedMesh;

            var sortedSides = SidesCalculator.CountsSides(mesh);

            var matrix = t.localToWorldMatrix;
            var verts = mesh.vertices.Select(x => matrix.MultiplyPoint3x4(x)).ToArray();

            var points = sortedSides.Select(x => x.Select(y => (Vector2)verts[y.x]).ToList()).ToList();
            var transform = t;
            var bindpose = t.worldToLocalMatrix;

            return new NotBonedPreCalculatedData(points, transform, bindpose);
        }

        private static BonedPreCalculatedData CalculateSkinnedPoints(Transform t)
        {
            var skmr = t.GetComponent<SkinnedMeshRenderer>();
            var mesh = skmr.sharedMesh;

            var sortedSides = SidesCalculator.CountsSides(mesh);

            var matrix = t.localToWorldMatrix;
            var matrixWtl = t.worldToLocalMatrix;
            var verts = mesh.vertices.Select(x => matrix.MultiplyPoint3x4(x)).ToArray();

            var boneWeights = mesh.boneWeights;

            var points = sortedSides.Select(x => x.Select(y => (Vector2)verts[y.x]).ToList()).ToList();
            var weights = sortedSides.Select(x => x.Select(y => new StarBW(boneWeights[y.x])).ToList()).ToList();
            var bones = skmr.bones;
            var bindposes = mesh.bindposes.Select(m => m * matrixWtl).ToArray();
            return new BonedPreCalculatedData(points, weights, bones, bindposes);
        }

        private class NotBonedPreCalculatedData
        {
            public List<List<Vector2>> points;
            public Transform parent;
            public Matrix4x4 bindpose;

            public NotBonedPreCalculatedData(List<List<Vector2>> points, Transform parent, Matrix4x4 bindpose)
            {
                this.points = points;
                this.parent = parent;
                this.bindpose = bindpose;
            }
        }

        private class BonedPreCalculatedData
        {
            public List<List<Vector2>> points;
            public List<List<StarBW>> weights;
            public Transform[] bones;
            public Matrix4x4[] bindposes;

            public BonedPreCalculatedData(List<List<Vector2>> points, List<List<StarBW>> weights, Transform[] bones,
                Matrix4x4[] bindposes)
            {
                this.points = points;
                this.weights = weights;
                this.bones = bones;
                this.bindposes = bindposes;
            }
        }

        private static Vector3 GetCenter(GroupData data)
        {
            var bounds = data.objects.Select(x => x.transform.GetComponent<Renderer>().bounds).ToArray();
            var minX = bounds.Select(x => x.min.x).Min();
            var minY = bounds.Select(x => x.min.y).Min();
            var maxX = bounds.Select(x => x.max.x).Max();
            var maxY = bounds.Select(x => x.max.y).Max();
            return new Vector3((minX + maxX) / 2, (minY + maxY) / 2, 0);
        }
    }
}
