﻿using System.Collections.Generic;
using UnityEngine;

namespace Fireworks
{
    public static class BWInterpolator
    {
        public static StarBW InterpolateBW(StarBW b1, StarBW b2, float f1)
        {
            var list = new List<KeyValuePair<int, float>>(12);
            AddBWToList(list, b1, f1);
            AddBWToList(list, b2, 1f - f1);
            SortList(list);
            float summ = 0;
            for (int i = 0; i < list.Count && i < 4; i++)
                summ += list[i].Value;

            return GetBWFromList(list, summ);
        }

        public static StarBW InterpolateBW(BoneWeight b1, BoneWeight b2, float f1)
        {
            var list = new List<KeyValuePair<int, float>>(12);
            AddBWToList(list, b1, f1);
            AddBWToList(list, b2, 1f - f1);
            SortList(list);
            float summ = 0;
            for (int i = 0; i < list.Count && i < 4; i++)
                summ += list[i].Value;

            return GetBWFromList(list, summ);
        }

        private static StarBW GetBWFromList(List<KeyValuePair<int, float>> list, float summ)
        {
            var bw = new StarBW {i0 = list[0].Key, w0 = list[0].Value / summ};
            if (list.Count > 1)
            {
                bw.i1 = list[1].Key;
                bw.w1 = list[1].Value / summ;
            }

            if (list.Count > 2)
            {
                bw.i2 = list[2].Key;
                bw.w2 = list[2].Value / summ;
            }

            if (list.Count > 3)
            {
                bw.i3 = list[3].Key;
                bw.w3 = list[3].Value / summ;
            }

            return bw;
        }

        static void SortList(List<KeyValuePair<int, float>> list)
        {
            for (int i = 0; i < list.Count; i++)
            for (int j = 0; j < list.Count - 1; j++)
            {
                if (list[j].Value < list[j + 1].Value)
                {
                    var kv = list[j];
                    list[j] = list[j + 1];
                    list[j + 1] = kv;
                }
            }
        }

        static void AddBWToList(List<KeyValuePair<int, float>> list, StarBW bw, float mult)
        {
            AddKWToList(list, bw.i0, bw.w0 * mult);
            if (bw.w1 != 0)
                AddKWToList(list, bw.i1, bw.w1 * mult);
            if (bw.w2 != 0)
                AddKWToList(list, bw.i2, bw.w2 * mult);
            if (bw.w3 != 0)
                AddKWToList(list, bw.i3, bw.w3 * mult);
        }

        static void AddBWToList(List<KeyValuePair<int, float>> list, BoneWeight bw, float mult)
        {
            AddKWToList(list, bw.boneIndex0, bw.weight0 * mult);
            if (bw.weight1 != 0)
                AddKWToList(list, bw.boneIndex1, bw.weight1 * mult);
            if (bw.weight2 != 0)
                AddKWToList(list, bw.boneIndex2, bw.weight2 * mult);
            if (bw.weight3 != 0)
                AddKWToList(list, bw.boneIndex3, bw.weight3 * mult);
        }

        public static void AddKWToList(List<KeyValuePair<int, float>> list, int index, float weight)
        {
            int indexAtList = GetIndex(list, index);
            if (indexAtList != -1)
                list[indexAtList] = new KeyValuePair<int, float>(index, list[indexAtList].Value + weight);
            else
                list.Add(new KeyValuePair<int, float>(index, weight));
        }

        public static int GetIndex(List<KeyValuePair<int, float>> list, int index)
        {
            for (int i = 0; i < list.Count; i++)
                if (list[i].Key == index)
                    return i;
            return -1;
        }
    }

    public static class BWMath
    {
        public static Vector3 CalculatePos(StarBW bw, Vector3 localPos, Matrix4x4[] frameMatrix)
        {
            var pos = frameMatrix[bw.i0].MultiplyPoint(localPos) * bw.w0;

            if (bw.w1 != 0)
                pos += frameMatrix[bw.i1].MultiplyPoint(localPos) * bw.w1;

            if (bw.w2 != 0)
                pos += frameMatrix[bw.i2].MultiplyPoint(localPos) * bw.w2;

            if (bw.w3 != 0)
                pos += frameMatrix[bw.i3].MultiplyPoint(localPos) * bw.w3;

            return pos;
        }
    }

    public struct StarBW
    {
        public int i0, i1, i2, i3;
        public float w0, w1, w2, w3;

        public StarBW(int i0, int i1, int i2, int i3, float w0, float w1, float w2, float w3)
        {
            this.i0 = i0;
            this.i1 = i1;
            this.i2 = i2;
            this.i3 = i3;
            this.w0 = w0;
            this.w1 = w1;
            this.w2 = w2;
            this.w3 = w3;
        }

        public StarBW(BoneWeight bw)
        {
            i0 = bw.boneIndex0;
            i1 = bw.boneIndex1;
            i2 = bw.boneIndex2;
            i3 = bw.boneIndex3;
            w0 = bw.weight0;
            w1 = bw.weight1;
            w2 = bw.weight2;
            w3 = bw.weight3;
        }
    }
}
