﻿using UnityEngine;

namespace Fireworks
{
    public static class PointHider
    {
        public static bool PointHided(Vector2 pos, Vector2[][] polygons)
        {
            if (polygons == null) return false;
            for (int i = 0; i < polygons.Length; i++)
                if (PointInPolygon(pos, polygons[i]))
                    return true;
            return false;
        }

        private static bool PointInPolygon(Vector2 uv, Vector2[] v)
        {
            bool b = false;
            int count = v.Length;
            for (int i = 0, j = count - 1; i < count; j = i++)
            {
                if (v[i].y > uv.y != v[j].y > uv.y &&
                    uv.x < (v[j].x - v[i].x) * (uv.y - v[i].y) / (v[j].y - v[i].y) + v[i].x)
                    b = !b;
            }

            return b;
        }

        private static bool PointInTriangle(Vector2 p, Vector2 a, Vector2 b, Vector2 c) =>
            MySide(p, a, b)
            && MySide(p, b, c)
            && MySide(p, c, a);

        private static bool MySide(Vector2 p, Vector2 a, Vector2 b) =>
            (b.x - a.x) * (p.y - a.y) < (b.y - a.y) * (p.x - a.x);
    }
}
