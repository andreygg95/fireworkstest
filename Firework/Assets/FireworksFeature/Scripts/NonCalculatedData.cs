﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Fireworks
{
    [Serializable]
    public class GroupData
    {
        public StarObject[] objects;
    }

    [Serializable]
    public class StarObject
    {
        public Transform transform;
        [FormerlySerializedAs("usedPolygons")] public string usedMeshParts;
        public int starIndex;
        public int lineWidth;
        [FormerlySerializedAs("hideTriangles")] public HidePolygonData[] hidePolygons;
    }
}
