﻿using System;
//using Sirenix.OdinInspector;
using UnityEngine;

namespace Fireworks
{
    [Serializable]
    public class HidePolygonData
    {
        public Transform transform;
        public Vector2[] points;

        [NonSerialized] public bool boned;
        [NonSerialized] public Transform[] bones;
        [NonSerialized] public Matrix4x4[] bindPoses;
        [NonSerialized] public StarBW[] weights;
        [NonSerialized] public Matrix4x4[] frameMatrix;

        public void SetBones(Transform[] bones, Matrix4x4[] bindPoses)
        {
            boned = true;
            this.bones = bones;
            this.bindPoses = bindPoses;
            frameMatrix = new Matrix4x4[bindPoses.Length];
        }

        public void CalculateWeights(Vector3[] vertices, BoneWeight[] vWeights)
        {
            weights = new StarBW[points.Length];

            for (var i = 0; i < points.Length; i++)
            {
                var point = points[i];
                var minIndex = 0;
                var minRange = Range(point, vertices[0]);

                for (int j = 1; j < vertices.Length; j++)
                {
                    var range = Range(point, vertices[j]);
                    if (minRange > range)
                    {
                        minRange = range;
                        minIndex = j;
                    }
                }

                weights[i] = new StarBW(vWeights[minIndex]);
            }
        }

//        [Button]
        public void GetFromPolygonCollider()
        {
            var pc = transform.GetComponent<PolygonCollider2D>();
            if (pc == null) return;
            points = pc.points;
        }

        private float Range(Vector2 a, Vector2 b)
        {
            var x = b.x - a.x;
            var y = b.y - a.y;
            return (float)Math.Sqrt(x * x + y * y);
        }

        private float PrimitiveRange(Vector2 a, Vector2 b)
        {
            var x = Mathf.Abs(b.x - a.x);
            var y = Mathf.Abs(b.y - a.y);
            return x > y ? x + y / 2 : y + x / 2;
        }
    }
}
