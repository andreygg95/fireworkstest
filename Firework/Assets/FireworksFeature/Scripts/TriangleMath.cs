﻿using UnityEngine;
using UnityEngine.Profiling;

namespace Fireworks
{
    public static class TriangleMath
    {
        public static void CalculatePolygon(ref Vector2[][] Polygon, HidePolygonData[] triangles)
        {
            if (triangles == null || triangles.Length == 0) return;
            if (Polygon == null) InitCalculatedPolygon(ref Polygon, triangles);

            Profiler.BeginSample("CalculateHTArray");

            for (var i = 0; i < triangles.Length; i++)
                CalculatePolygon(triangles[i], Polygon[i]);

            Profiler.EndSample();
        }

        private static void CalculatePolygon(HidePolygonData tria, Vector2[] poly)
        {
            if (tria.boned)
                CalculateBonedPolygon(tria, poly);
            else
                CalculateNotBonedPolygon(tria, poly);
        }

        private static void CalculateBonedPolygon(HidePolygonData tria, Vector2[] poly)
        {
            for (int i = 0; i < tria.frameMatrix.Length; i++)
                tria.frameMatrix[i] = tria.bones[i].localToWorldMatrix * tria.bindPoses[i];

            for (int i = 0; i < tria.points.Length; i++)
                poly[i] = CalculateVertexPos(tria.weights[i], tria.points[i], tria.frameMatrix);
        }

        private static Vector3 CalculateVertexPos(StarBW bw, Vector3 localPos, Matrix4x4[] frameMatrix)
        {
            var pos = frameMatrix[bw.i0].MultiplyPoint(localPos) * bw.w0;

            if (bw.w1 != 0)
                pos += frameMatrix[bw.i1].MultiplyPoint(localPos) * bw.w1;

            if (bw.w2 != 0)
                pos += frameMatrix[bw.i2].MultiplyPoint(localPos) * bw.w2;

            if (bw.w3 != 0)
                pos += frameMatrix[bw.i3].MultiplyPoint(localPos) * bw.w3;

            return pos;
        }

        private static void CalculateNotBonedPolygon(HidePolygonData tria, Vector2[] poly)
        {
            var matrix = tria.transform != null ? tria.transform.localToWorldMatrix : Matrix4x4.identity;

            for (int j = 0; j < tria.points.Length; j++)
                poly[j] = matrix.MultiplyPoint(tria.points[j]);
        }

        private static void InitCalculatedPolygon(ref Vector2[][] Polygon, HidePolygonData[] triangles)
        {
            Polygon = new Vector2[triangles.Length][];
            for (var i = 0; i < triangles.Length; i++)
                Polygon[i] = new Vector2[triangles[i].points.Length];
        }
    }
}
