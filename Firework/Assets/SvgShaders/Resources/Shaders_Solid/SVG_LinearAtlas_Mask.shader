Shader "SVG/SolidLinearAtlas/Mask" {
	
	Properties {
		_GradientColor ("Gradients Tex (RGBA)", 2D) = "white" { }
		_MaskTex ("Mask Tex", 2D) = "white" { } 
		_PosSize ("Left Down Point & tex size", Vector) = (-100,-100,200,200)
	}
	

	SubShader
	{
		Tags { "Queue"="Geometry" "RenderType"="Opaque" }
		LOD 200
		Lighting Off
		Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
		ZWrite On
		ZTest LEqual
		Cull Off
		Fog { Mode Off }

		Pass
		{
			CGPROGRAM
			#pragma vertex vertexGradients
			#pragma fragment fragmentColor
			#include "UnityCG.cginc"
			#include "SVG_Lib.cginc"
			
			struct vertex_input	{ VERT_INPUT_LINEARATLAS };
			
			struct vertex_output { VERT_OUTPUT_LINEARATLAS_P };

			DECLARE_MASK
			DECLARE_GRAD
	
			vertex_output vertexGradients(vertex_input v) { VERT_CODE_LINEARATLAS_M }
		
			float4 fragmentColor(vertex_output i) : COLOR { FRAC_LINEARATLAS FRAC_MASK FRAC_RET }
			ENDCG
        }
	}
}