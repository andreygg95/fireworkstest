Shader "SVG/SolidComplex/Alpha" {
	
	Properties {
		_GradientColor ("Gradients Tex (RGBA)", 2D) = "white" { }
		_MainTex ("Pixels Atlas", 2D) = "white" { }
		_Alpha("Alpha", Range(0,1))=1
	}
	

	SubShader
	{
		Tags { "Queue"="Geometry" "RenderType"="Opaque" }
		LOD 200
		Lighting Off
		Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
		ZWrite On
		ZTest LEqual
		Cull Off
		Fog { Mode Off }

		Pass
		{
			CGPROGRAM
			#pragma vertex vertexGradients
			#pragma fragment fragmentColor
			#include "UnityCG.cginc"
			#include "SVG_Lib.cginc"
			
			struct vertex_input	{ VERT_INPUT_COMPLEX };
			
			struct vertex_output { VERT_OUTPUT_COMPLEX };

			DECLARE_GRAD
			fixed _Alpha;

			vertex_output vertexGradients(vertex_input v) { VERT_CODE_COMPLEX }
		
			float4 fragmentColor(vertex_output i) : COLOR { FRAC_COMPLEX c.a *= _Alpha; FRAC_RET }
			ENDCG
        }
	}
}