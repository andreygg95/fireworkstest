Shader "SVG/SolidLinearAtlas/Default" {
	
	Properties {
		_GradientColor ("Gradients Tex (RGBA)", 2D) = "white" { }
	}
	

	SubShader
	{
		Tags { "Queue"="Geometry" "RenderType"="Opaque" }
		LOD 200
		Lighting Off
		Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
		ZWrite On
		ZTest LEqual
		Cull Off
		Fog { Mode Off }

		Pass
		{
			CGPROGRAM
			#pragma vertex vertexGradients
			#pragma fragment fragmentColor
			#include "UnityCG.cginc"
			#include "SVG_Lib.cginc"
			
			struct vertex_input	{ VERT_INPUT_LINEARATLAS };
			
			struct vertex_output { VERT_OUTPUT_LINEARATLAS };

			DECLARE_GRAD
	
			vertex_output vertexGradients(vertex_input v) { VERT_CODE_LINEARATLAS }
		
			float4 fragmentColor(vertex_output i) : COLOR { FRAC_LINEARATLAS FRAC_RET }
			ENDCG
        }
	}
}