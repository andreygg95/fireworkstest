Shader "SVG/Complex/Colorize" {
	
	Properties {
		_GradientColor ("Gradients Tex (RGBA)", 2D) = "white" { }
		_MainTex ("Pixels Atlas", 2D) = "white" { }
		_Color ("Main Color", Color) = (1,1,1,1)
		_Influence ("Influence", Range(0, 1)) = 1
	}
	

	SubShader
	{
		Tags {"RenderType"="Transparent" "Queue"="Transparent"}
		LOD 200
		Lighting Off
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off
		Cull Off
		Fog { Mode Off }

		Pass
		{
			CGPROGRAM
			#pragma vertex vertexGradients
			#pragma fragment fragmentColor
			#include "UnityCG.cginc"
			#include "SVG_Lib.cginc"
			
			struct vertex_input	{ VERT_INPUT_COMPLEX };
			
			struct vertex_output { VERT_OUTPUT_COMPLEX };

			DECLARE_GRAD
			DECLARE_MAIN

			half4 _Color;
			float _Influence;

			vertex_output vertexGradients(vertex_input v) { VERT_CODE_COMPLEX }
		float w3c_luma(float3 c) {
				return dot(c, float3(0.299, 0.587, 0.114));
			}
			float _epsilon = 1e-5;
			float4 fragmentColor(vertex_output i) : COLOR { FRAC_COMPLEX 
			fixed4 texcol = c;
				float3 C0 = texcol.rgb;
					float3 C1 = _Color.rgb;
					float L0 = w3c_luma(C0);
					float L1 = w3c_luma(C1);
					float3 C = C1 + (L0-L1);
					float3 t3 = max(C-1, -C)/(abs(L0-C) + _epsilon);
					float t = max(max(max(t3.x, t3.y), t3.z), 0);
					fixed4 fc = fixed4(lerp(C, float3(L0, L0, L0), t), texcol.a * _Color.a);
					c = lerp(texcol, fc, _Influence);
			FRAC_RET }
			ENDCG
        }
	}
}