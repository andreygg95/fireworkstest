Shader "SVG/LinearAtlas/HSV" {
	
	Properties {
		_GradientColor ("Gradients Tex (RGBA)", 2D) = "white" { }
		_H ("Hue", Float) = 0
		_S ("Saturation", Float) = 0
		_V ("Value", Float) = 0
	}
	

	SubShader
	{
		Tags {"RenderType"="Transparent" "Queue"="Transparent"}
		LOD 200
		Lighting Off
		Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
		ZWrite Off
		Cull Off
		Fog { Mode Off }

		Pass
		{
			CGPROGRAM
			#pragma vertex vertexGradients
			#pragma fragment fragmentColor
			#include "UnityCG.cginc"
			#include "SVG_Lib.cginc"
			
			struct vertex_input	{ VERT_INPUT_LINEARATLAS };
			
			struct vertex_output { VERT_OUTPUT_LINEARATLAS };

			DECLARE_GRAD
			DECLARE_HSV

			vertex_output vertexGradients(vertex_input v) { VERT_CODE_LINEARATLAS }
		
			float4 fragmentColor(vertex_output i) : COLOR { FRAC_LINEARATLAS FRAC_HSV FRAC_RET }
			ENDCG
        }
	}
}