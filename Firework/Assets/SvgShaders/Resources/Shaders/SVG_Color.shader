Shader "SVG/Color" {
	
	Properties {
	_Color ("Color", Color) = (1,1,1,1)
	}
	
	SubShader
	{
		Tags {"RenderType"="Transparent" "Queue"="Transparent"}
		LOD 200	
		Lighting Off	
		Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
		ZWrite Off
		Cull Off
		Fog { Mode Off }	
		
		Pass
		{			
			CGPROGRAM
			#pragma vertex vertexColor
			#pragma fragment fragmentColor
			#include "UnityCG.cginc"
			#include "SVG_Lib.cginc"

			struct vertex_input { VERT_INPUT_V };
			
			struct vertex_output { VERT_OUTPUT_V };	
			
			vertex_output vertexColor(vertex_input v) { VERT_CODE_V }

			fixed4 _Color;	

			half4 fragmentColor(vertex_output i) : COLOR { return _Color; }				
			ENDCG
        }
	}
}
