Shader "SVG/Complex/Multiply" {
	
	Properties {
		_GradientColor ("Gradients Tex (RGBA)", 2D) = "white" { }
		_MainTex ("Pixels Atlas", 2D) = "white" { }
		_Color ("Color", Color) = (1,1,1,1)
	}
	

	SubShader
	{
		Tags {"RenderType"="Transparent" "Queue"="Transparent"}
		LOD 200
		Lighting Off
		Blend Zero SrcColor, One One
		BlendOp Add
		ZWrite Off
		Cull Off
		Fog { Mode Off }

		Pass
		{
			CGPROGRAM
			#pragma vertex vertexGradients
			#pragma fragment fragmentColor
			#include "UnityCG.cginc"
			#include "SVG_Lib.cginc"
			
			struct vertex_input	{ VERT_INPUT_COMPLEX };
			
			struct vertex_output { VERT_OUTPUT_COMPLEX };

			DECLARE_GRAD
			DECLARE_MAIN
			DECLARE_COLOR

			vertex_output vertexGradients(vertex_input v) { VERT_CODE_COMPLEX }
		
			float4 fragmentColor(vertex_output i) : COLOR { FRAC_COMPLEX FRAC_MULTIPLY FRAC_RET }
			ENDCG
        }
	}
}