Shader "SVG/Complex/MaskRainbow" {

	Properties {
		_GradientColor ("Gradients Tex (RGBA)", 2D) = "white" { }
		_MainTex ("Pixels Atlas", 2D) = "white" { }
		_MaskTex ("Mask Tex", 2D) = "white" { }
		_PosSize ("Left Down Point & tex size", Vector) = (-100,-100,200,200)
		_Angle ("Mask rotate angle", float) = 0
		_Color ("Lerp Color for mask", Color) = (1,1,1,1)
	}


	SubShader
	{
		Tags {"RenderType"="Transparent" "Queue"="Transparent"}
		LOD 200
		Lighting Off
		Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
		ZWrite Off
		Cull Off
		Fog { Mode Off }

		Pass
		{
			CGPROGRAM
			#pragma vertex vertexGradients
			#pragma fragment fragmentColor
			#include "UnityCG.cginc"
			#include "SVG_Lib.cginc"

			struct vertex_input	{ VERT_INPUT_COMPLEX };

			struct vertex_output { VERT_OUTPUT_COMPLEX_P };

			DECLARE_MASK
			DECLARE_GRAD
			DECLARE_COLOR
			float _Angle;

			vertex_output vertexGradients(vertex_input v) { VERT_CODE_COMPLEX_MR }

			float4 fragmentColor(vertex_output i) : COLOR { FRAC_COMPLEX FRAC_MASK_Rainbow FRAC_RET }
			ENDCG
        }
	}
}
