

#define DECLARE_GRAD sampler2D _GradientColor; float4 _GradientColor_TexelSize;
#define DECLARE_MAIN sampler2D _MainTex; float4 _MainTex_TexelSize;
#define DECLARE_PATTERN sampler2D _MaskTex; float4 _PosSize; fixed4 _Color;
#define DECLARE_MASK sampler2D _MaskTex; float4 _PosSize;
#define DECLARE_GRADPATTERN sampler2D _MaskTex; float4 _PosSize; fixed4 _Color; fixed4 _ColorTo;
#define DECLARE_COLOR fixed4 _Color;
#define DECLARE_HSV float _H, _S, _V;
#define DECLARE_SIZE float _Size;
#define DECLARE_SHIFT float _Shift;

#define VERT_INPUT_V float4 vertex : POSITION;
#define VERT_INPUT_LINEARATLAS float4 vertex : POSITION; float2 texcoord0 : TEXCOORD0;
#define VERT_INPUT_COMPLEX float4 vertex : POSITION; float2 texcoord0 : TEXCOORD0; float2 texcoord1 : TEXCOORD1;

#define VERT_OUTPUT_V float4 vertex : SV_POSITION;
#define VERT_OUTPUT_LINEARATLAS float4 vertex : SV_POSITION; float2 uv0 : TEXCOORD0;
#define VERT_OUTPUT_LINEARATLAS_P float4 vertex : SV_POSITION; float2 uv0 : TEXCOORD0; float2 uv1 : TEXCOORD1;
#define VERT_OUTPUT_COMPLEX float4 vertex : SV_POSITION; float2 uv0 : TEXCOORD0; float2 uv2 : TEXCOORD1;
#define VERT_OUTPUT_COMPLEX_P float4 vertex : SV_POSITION; float2 uv0 : TEXCOORD0; float2 uv1 : TEXCOORD1; float2 uv2 : TEXCOORD2;

#define VERT_CODE_V vertex_output o; o.vertex = UnityObjectToClipPos(v.vertex); return o;

#define VERT_CODE_COMPLEX vertex_output o;\
	o.vertex = UnityObjectToClipPos(v.vertex);\
	o.uv0 = v.texcoord0;\
	o.uv2 = float2(v.texcoord1.x, (v.texcoord1.y * 2 + 1) * _GradientColor_TexelSize.y);\
	return o;

#define VERT_CODE_LINEARATLAS vertex_output o;\
	o.vertex = UnityObjectToClipPos(v.vertex);\
	o.uv0 = fixed2(v.texcoord0.x, (v.texcoord0.y * 2 + 1) * _GradientColor_TexelSize.y);\
	return o;

#define VERT_CODE_COMPLEX_P vertex_output o;\
	o.vertex = UnityObjectToClipPos(v.vertex);\
	o.uv0 = v.texcoord0;\
	float4 abs_pos = mul(unity_ObjectToWorld, v.vertex);\
	float4 rel_pos = mul(unity_WorldToObject, abs_pos);\
	o.uv1 = (rel_pos.xy - _PosSize.xy) / _PosSize.zw;\
	o.uv2 = float2(v.texcoord1.x, (v.texcoord1.y * 2 + 1) * _GradientColor_TexelSize.y);\
	return o;

#define VERT_CODE_LINEARATLAS_P vertex_output o;\
	o.vertex = UnityObjectToClipPos(v.vertex);\
	o.uv0 = fixed2(v.texcoord0.x, (v.texcoord0.y * 2 + 1) * _GradientColor_TexelSize.y);\
	float4 abs_pos = mul(unity_ObjectToWorld, v.vertex);\
	float4 rel_pos = mul(unity_WorldToObject, abs_pos);\
	o.uv1 = (rel_pos.xy - _PosSize.xy) / _PosSize.zw;\
	return o;

#define VERT_CODE_COMPLEX_M vertex_output o;\
	o.vertex = UnityObjectToClipPos(v.vertex);\
	o.uv0 = v.texcoord0;\
	float4 abs_pos = mul(unity_ObjectToWorld, v.vertex);\
	o.uv1 = (abs_pos.xy - _PosSize.xy) / _PosSize.zw;\
	o.uv2 = float2(v.texcoord1.x, (v.texcoord1.y * 2 + 1) * _GradientColor_TexelSize.y);\
	return o;

	#define VERT_CODE_COMPLEX_MR vertex_output o;\
	o.vertex = UnityObjectToClipPos(v.vertex);\
	o.uv0 = v.texcoord0;\
	o.uv2 = float2(v.texcoord1.x, (v.texcoord1.y * 2 + 1) * _GradientColor_TexelSize.y);\
	float radAngle = _Angle / 180 * 3.1415926535897932384626433832795;\
	float si = sin(radAngle);\
	float co = cos(radAngle);\
	float4 abs_pos = mul(unity_ObjectToWorld, v.vertex);\
	float2 uv1 = (abs_pos.xy - _PosSize.xy) / _PosSize.zw;\
	o.uv1 = float2(uv1.x * co + uv1.y * si, -uv1.x * si + uv1.y * co);\
	return o;

#define VERT_CODE_LINEARATLAS_M vertex_output o;\
	o.vertex = UnityObjectToClipPos(v.vertex);\
	o.uv0 = fixed2(v.texcoord0.x, (v.texcoord0.y * 2 + 1) * _GradientColor_TexelSize.y);\
	float4 abs_pos = mul(unity_ObjectToWorld, v.vertex);\
	o.uv1 = (abs_pos.xy - _PosSize.xy) / _PosSize.zw;\
	return o;


#define FRAC_MULTCOLOR c *= _Color;
#define FRAC_HSV float3 tmp = rgb2hsv(c.rgb); tmp.x += _H; tmp.x -= floor(tmp.x); tmp.y = saturate(tmp.y + _S); tmp.z = saturate(tmp.z + _V); c.rgb = hsv2rgb(tmp);
#define FRAC_MULTIPLY c.a *= _Color.a; c = fixed4(lerp(fixed3(1,1,1), c.rgb, c.a), 0);
#define FRAC_MASK c.a *= tex2D(_MaskTex, i.uv1).a * (i.uv1.x>0 && i.uv1.x < 1 && i.uv1.y > 0 && i.uv1.y < 1);

#define FRAC_MASK_Rainbow c.a *= tex2D(_MaskTex, i.uv1).a;
#define FRAC_MASK_Rainbow1 c.rgb = lerp(c.rgb, _Color.rgb, (1-tex2D(_MaskTex, i.uv1).a) * _Color.a);

#define FRAC_RET return c;
#define FRAC_TONE fixed factor_minmax = (max(max(c.r, c.g), c.b) + min(min(c.r, c.g), c.b)) * 0.5; fixed factor_dot = dot(c.rgb, _Grayscale.rgb); fixed factor_len = length(c.rgb * _Grayscale.rgb); c.rgb = dot(fixed3(factor_minmax, factor_dot, factor_len), fixed3(_Grayscale.a < 0, _Grayscale.a == 0, _Grayscale.a > 0));


#define FRAC_COMPLEX		fixed f = length(i.uv0);\
							fixed4 c = tex2D(_GradientColor, fixed2(lerp(i.uv0.x, f, i.uv2.x), i.uv2.y));

#define FRAC_LINEARATLAS	fixed4 c = tex2D(_GradientColor, i.uv0);

#define FRAC_COMPLEX_old	 FRAC_COMPLEX c += tex2D(_MainTex, fixed2(i.uv3, 0.5));
#define FRAC_GRADATLAS		 FRAC_COMPLEX
#define FRAC_COLOR_GRADATLAS FRAC_COMPLEX c *= i.color;

#define FRAC_SHIFT i.uv1.x -= _Shift * floor(i.uv1.y);
#define FRAC_SIZE i.uv1 = clamp(frac(i.uv1) * _Size, fixed2(0, 0), fixed2(1, 1));
#define FRAC_SS FRAC_SHIFT FRAC_SIZE

#define FRAC_TEXPATTERN fixed4 tc = tex2D(_MaskTex, i.uv1); c.rgb = lerp(c.rgb, tc.rgb, _Color.a * tc.a);

#define FRAC_COLORPATTERN c.rgb = lerp(c.rgb, _Color, tex2D(_MaskTex, i.uv1).a * _Color.a);

#define FRAC_GRADPATTERN _Color = lerp(_Color, _ColorTo, length(frac(i.uv1) - fixed2(0.5,0.5)) * 2);

float3 rgb2hsv(float3 c) {
    const float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    float4 p = c.g < c.b ? float4(c.bg, K.wz) : float4(c.gb, K.xy);
    float4 q = c.r < p.x ? float4(p.xyw, c.r) : float4(c.r, p.yzx);
    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return float3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}
float3 hsv2rgb(float3 c) {
    float4 K = float4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    float3 p = abs(frac(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * lerp(K.xxx, saturate(p - K.xxx), c.y);
}
