Shader "SVG/Complex/Pattern_Texture" {
	
	Properties {
		_GradientColor ("Gradients Tex (RGBA)", 2D) = "white" { }
		_MainTex ("Pixels Atlas", 2D) = "white" { }
		_MaskTex ("Mask Tex", 2D) = "white" { } 
		_PosSize ("Left Down Point & tex size", Vector) = (0,0,32,32)
		_Color("Pattern Color", Color) = (1,1,1,1)
		_Size("Size Multiplier", Range(1,3)) = 1.1
		_Shift("X Shift", Range(0,1)) = 0.1
	}
	

	SubShader
	{
		Tags {"RenderType"="Transparent" "Queue"="Transparent"}
		LOD 200
		Lighting Off
		Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
		ZWrite Off
		Cull Off
		Fog { Mode Off }

		Pass
		{
			CGPROGRAM
			#pragma vertex vertexGradients
			#pragma fragment fragmentColor
			#include "UnityCG.cginc"
			#include "SVG_Lib.cginc"
			
			struct vertex_input	{ VERT_INPUT_COMPLEX };
			
			struct vertex_output { VERT_OUTPUT_COMPLEX_P };

			DECLARE_GRAD
			DECLARE_PATTERN
			DECLARE_SIZE
			DECLARE_SHIFT

			vertex_output vertexGradients(vertex_input v) { VERT_CODE_COMPLEX_P }
		
			float4 fragmentColor(vertex_output i) : COLOR {	FRAC_COMPLEX FRAC_SS FRAC_TEXPATTERN FRAC_RET }
			ENDCG
        }
	}
}