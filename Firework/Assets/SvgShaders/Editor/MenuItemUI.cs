﻿using UnityEditor;
using UnityEngine;

namespace Bini.ToolKit.SvgShaders.Editor
{
	public class MenuItemUI
	{
		public const string Group = "SVG/Shader GUIDs/";

		private readonly string name;
		private readonly string path;

		public MenuItemUI(string menuItem)
		{
			this.name = menuItem;
			this.path = Group + menuItem;

			Debug.Log($"--------> Menu <b>{path}</b>");
		}

		public bool ShowStartDialog(string message)
		{
			var response = EditorUtility.DisplayDialog(name + " - Start",
				$"{message}\n\nIt might take some time. Do you want to proceed?",
				"Yes", "No");

			if (!response)
				LogStop("User cancelled start dialog");

			return response;
		}

		public void ShowErrorDialog(string error)
		{
			EditorUtility.DisplayDialog(name + " - Error", error, "Ok");
			LogStop(error);
		}

		public void ShowFinishDialog(string message)
		{
			EditorUtility.DisplayDialog(name + " - Done!",
				$"{message}\n\nSee all the details in the Console window.", "Ok");
			Debug.Log(message);
			LogStop();
		}

		private void LogStop(string error = "")
		{
			if (!string.IsNullOrEmpty(error))
				Debug.LogError(error);

			Debug.Log($"<-------- Menu <b>{path}</b>");
		}
	}
}
