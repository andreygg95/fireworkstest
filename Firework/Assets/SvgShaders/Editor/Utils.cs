﻿using System.Collections;
using System.IO;
using System.Linq;
using UnityEditor;

namespace Bini.ToolKit.SvgShaders.Editor
{
	public static class Utils
	{
		public const int GuidLength = 32;

		public static string GetSelectedTxtFilePath()
		{
			if (Selection.activeObject == null)
				return string.Empty;

			var path = AssetDatabase.GetAssetPath(Selection.activeObject);

			if (string.IsNullOrEmpty(path) || Path.GetExtension(path) != ".txt")
				return string.Empty;

			return path;
		}

		public static bool IsGuid(string text)
		{
			return !string.IsNullOrEmpty(text)
			       && text.Length == GuidLength
				   && text.All(c => (c >= '0' && c <= '9') || (c >= 'a' && c <= 'z'));
		}

		public static string GetPluralPrefix(ICollection collection)
		{
			return collection.Count == 1 ? "" : "s";
		}
	}
}
