﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;

namespace Bini.ToolKit.SvgShaders.Editor
{
	// TODO: This is a temporary solution to make sure SVG shader guids are the same in all projects.
	// As soon as the problem is eliminated, script should either be removed,
	// or reworked to perform wider range of tasks.
	public class GuidRewriter : EditorWindow
	{
		public static GuidRewriter Window = null;
		static bool Initialized => Window != null;

		public string[] from, to;

		private const string shaderStart = @"  m_Shader: ";
		private const string scriptStart = @"  m_Script: ";

		private string[] SvgShadersLegacy =
		{
			"561ab8ef47ccbc9478975b2557bd1351", //SVG_Color
			"0c1281db6a6ac894b9e9813319439447", //SVG_Complex
			"0438d83bb1e4fbf44b405a89b1b567fe", //SVG_Complex_Alpha
			"04ca7ce5e8d3f5747abd30fc4ed00c38", //SVG_Complex_Colorize
			"09e1264921e0f5c4a9c4bc8ba6c3a9c9", //SVG_Complex_HSV
			"aadd4b211c857ce46879238c1792b4bb", //SVG_Complex_Mask
			"42635c9d7c95c154791b95fd65740f3f", //SVG_Complex_Multiply
			"5e95284aec0bb074e83291e45e8f07da", //SVG_Complex_Pattern_Color
			"68c72b79cf315d14c8c47ed1c4aff979", //SVG_Complex_Pattern_Texture
			"0c6655fcd4e3fe0418b81c4349485e86", //SVG_LinearAtlas
			"35c5dcb32ed3feb4e8332475f57f5e5d", //SVG_LinearAtlas_Alpha
			"a42ce70a3a3ea6449952a54002f17f69", //SVG_LinearAtlas_HSV
			"05f3c12670f196a46bae5f1ed8664cdd", //SVG_LinearAtlas_Mask
			"693fc134b2ea1a2478655da33abde531", //SVG_LinearAtlas_Multiply
			"05452d4e1f41fb9489932c48e7d56f65", //SVG_LinearAtlas_Pattern_Color
			"fc3f66ae396fab34c885f99b16a5c9af" //SVG_LinearAtlas_Pattern_Texture
		};

		private string[] SvgShadersSimple =
		{
			"0eb312d67d072734c9f273f8bc336275", //SVG_Color
			"15d6917d6da7add419994338c801f9ff", //SVG_Complex
			"837e621f72e5f9640b209419c0e2a984", //SVG_Complex_Alpha
			"f2788b6d834a40e4ebb561e98ec28bd7", //SVG_Complex_Colorize
			"34d352a554a54934b91c9971511450b4", //SVG_Complex_HSV
			"602d8c054eec40047bea6061af2bfc10", //SVG_Complex_Mask
			"26d7b33c5cd591b4d852a94b2c68cb70", //SVG_Complex_Multiply
			"05d8477eba910e6418d8a11038282da1", //SVG_Complex_Pattern_Color
			"b1211b5e2facb2a4db8aaf145bf5dc90", //SVG_Complex_Pattern_Texture
			"22cb38d901bf02b42bd879a105496b99", //SVG_LinearAtlas
			"f5ba7b70b549ce043bb345afab02b84d", //SVG_LinearAtlas_Alpha
			"1a0c4269da695d246a5297a4a12b3578", //SVG_LinearAtlas_HSV
			"e9c1ce7f052b8da4d89c54583dc24055", //SVG_LinearAtlas_Mask
			"3bc2980727a319f4eb1e0d3f9f77ac55", //SVG_LinearAtlas_Multiply
			"53d58397708af6e4a8fe4c89bd6637be", //SVG_LinearAtlas_Pattern_Color
			"4fd9df46a754d294ebc4951239bdd62c" //SVG_LinearAtlas_Pattern_Texture
		};

		GuidRewriter()
		{
			Window = this;
		}

		static GuidRewriter()
		{
			Init(true);
		}

		[MenuItem("SVG/Shader GUIDs/GUID Rewriter", priority = 1)]
		public static void InitFromMenu()
		{
			Init(false);
		}

		public static void Init(bool windowExists = false)
		{
			if (Initialized) return;

			if (!windowExists)
			{
				Window = GetWindow<GuidRewriter>();
				Window.titleContent = new GUIContent("Guid rewriter");
				Window.autoRepaintOnSceneChange = true;
				Window.from = new string[1];
				Window.to = new string[1];
			}
		}

		private void OnGUI()
		{
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("ArraySize");
			var arraySize = EditorGUILayout.IntField(from.Length);
			EditorGUILayout.EndHorizontal();

			if (arraySize != from.Length)
				System.Array.Resize(ref from, arraySize);

			if (arraySize != to.Length)
				System.Array.Resize(ref to, arraySize);

			EditorGUILayout.BeginHorizontal();

			EditorGUILayout.BeginVertical();

			EditorGUILayout.LabelField("Guid from:");
			for (int i = 0; i < arraySize; i++) from[i] = EditorGUILayout.TextField(from[i]);

			EditorGUILayout.EndVertical();

			EditorGUILayout.BeginVertical();

			EditorGUILayout.LabelField("Guid to:");
			for (int i = 0; i < arraySize; i++) to[i] = EditorGUILayout.TextField(to[i]);

			EditorGUILayout.EndVertical();

			EditorGUILayout.EndHorizontal();

			if (arraySize < 1) return;
			if (GUILayout.Button("Fix Materials"))
				RewriteMaterials();

			if (GUILayout.Button("Fix Scenes"))
				RewriteScenes();

			if (GUILayout.Button("Get Legacy To Simple"))
			{
				from = new string[SvgShadersLegacy.Length];
				to = new string[SvgShadersLegacy.Length];
				System.Array.Copy(SvgShadersLegacy, from, from.Length);
				System.Array.Copy(SvgShadersSimple, to, from.Length);
			}

			if (GUILayout.Button("Get Simple To Legacy"))
			{
				from = new string[SvgShadersLegacy.Length];
				to = new string[SvgShadersLegacy.Length];
				System.Array.Copy(SvgShadersSimple, from, from.Length);
				System.Array.Copy(SvgShadersLegacy, to, from.Length);
			}
		}

		public void RewriteMaterials()
		{
			AssetDatabase.StartAssetEditing();
			string projPath = Application.dataPath.Replace("Assets", "");
			Material[] mats = Selection.GetFiltered<Material>(SelectionMode.DeepAssets);
			for (var i = 0; i < mats.Length; i++)
			{
				Progress("Writing Materials", "Progress", i, mats.Length);
				var mat = mats[i];
				Rewrite(projPath + AssetDatabase.GetAssetPath(mat), shaderStart);
			}

			ClearBar();
			AssetDatabase.StopAssetEditing();
			AssetDatabase.Refresh();
		}

		public void RewriteScenes()
		{
			AssetDatabase.StartAssetEditing();
			string projPath = Application.dataPath.Replace("Assets", "");
			SceneAsset[] mats = Selection.GetFiltered<SceneAsset>(SelectionMode.DeepAssets);
			for (var i = 0; i < mats.Length; i++)
			{
				Progress("Writing Scenes", "Progress", i, mats.Length);
				var mat = mats[i];
				Rewrite(projPath + AssetDatabase.GetAssetPath(mat), scriptStart);
			}

			ClearBar();
			AssetDatabase.StopAssetEditing();
			AssetDatabase.Refresh();
		}

		public void Rewrite(string path, string start)
		{
			var data = File.ReadAllLines(path);
			bool isDirty = false;
			for (int i = 0; i < data.Length; i++)
			{
				var s = data[i];
				if (s.StartsWith(start))
					for (int j = 0; j < from.Length; j++)
						if (s.Contains(from[j]))
						{
							s = s.Replace(from[j], to[j]);
							isDirty = true;
						}

				data[i] = s;
			}

			if (isDirty)
				File.WriteAllLines(path, data);
		}

		public static void Progress(string title, string text, int n, int count)
		{
			EditorUtility.DisplayProgressBar(title, string.Format(text + " {0} / {1}", n, count), (float)n / count);
		}

		public static void ClearBar()
		{
			EditorUtility.ClearProgressBar();
		}
	}
}
