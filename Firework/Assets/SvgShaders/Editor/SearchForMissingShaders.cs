/*
 * This script iterates over materials in the given folder, and determines
 * whether shader GUIDs are corresponding to existing shaders.
 *
 * Unidentified GUIDs are written to .txt file placed under selected directory,
 * and can be used to run SearchForShadersByGuid script. List of materials involved
 * is also saved to another .txt file providing details on missing shaders usage.
 * Search process is also logged into Console window.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Bini.ToolKit.SvgShaders.Editor
{
	// TODO: This is a temporary solution to make sure SVG shader guids are the same in all projects - see TK-282.
	// As soon as the problem is eliminated, script should either be removed,
	// or reworked to perform wider range of tasks.
	public class SearchForMissingShaders
	{
		private const string MenuItemName = "Search For Missing Shaders";
		private const string OutputFileName = "MissingShaderGUIDs.txt";
		private const string UsageOutputFileName = "MissingShadersUsage.txt";

		[MenuItem(MenuItemUI.Group + MenuItemName, priority = 2)]
		private static void Run()
		{
			var menuItemUI = new MenuItemUI(MenuItemName);

			var selectionPath = GetSelectedFolderPath();
			if (string.IsNullOrEmpty(selectionPath))
			{
				menuItemUI.ShowErrorDialog("First select target folder in the Project window!");
				return;
			}

			if (!PerformSearch(selectionPath, menuItemUI, out var searchOutput))
				return;

			Debug.Log("Listing obsolete shader references:");

			var usageOutput = new StringBuilder();

			foreach (var guid in searchOutput)
			{
				var message = new StringBuilder();
				message.AppendLine($"guid: {guid.Key} - {guid.Value.Count} material{Utils.GetPluralPrefix(guid.Value)}:");

				foreach (var filePath in guid.Value)
				{
					message.AppendLine($" - {filePath}");
				}

				Debug.Log(message);

				usageOutput.Append(message).AppendLine();
			}

			var usageOutputFilePath = selectionPath + Path.DirectorySeparatorChar + UsageOutputFileName;
			File.WriteAllText(usageOutputFilePath, usageOutput.ToString());

			var outputFilePath = selectionPath + Path.DirectorySeparatorChar + OutputFileName;
			File.WriteAllLines(outputFilePath, searchOutput.Keys);

			AssetDatabase.Refresh();

			menuItemUI.ShowFinishDialog($"All missing shader GUIDs are saved to {outputFilePath}, " +
			                            $"and materials involved are listed in {usageOutputFilePath}");
		}

		private static bool PerformSearch(string selectionPath, MenuItemUI menuItemUI,
			out Dictionary<string, List<string>> output)
		{
			if (!menuItemUI.ShowStartDialog("We're going to look over all the materials " +
			                                $"in selected folder:\n\n{selectionPath}"))
			{
				output = null;
				return false;
			}

			var materials = Selection.GetFiltered<Material>(SelectionMode.DeepAssets);
			var unidentifiedGuids = new Dictionary<string, List<string>>();

			foreach (var asset in materials)
			{
				var assetPath = AssetDatabase.GetAssetPath(asset);

				var guid = GetShaderGuidFromMaterial(assetPath);
				if (string.IsNullOrEmpty(guid) || !string.IsNullOrEmpty(AssetDatabase.GUIDToAssetPath(guid)))
					continue;

				if (!unidentifiedGuids.ContainsKey(guid))
					unidentifiedGuids.Add(guid, new List<string>());

				unidentifiedGuids[guid].Add(assetPath);
			}

			if (unidentifiedGuids.Count == 0)
			{
				menuItemUI.ShowFinishDialog("No missing shader references were found!");
				output = null;
				return false;
			}

			output = unidentifiedGuids;
			return true;
		}

		private static string GetShaderGuidFromMaterial(string assetPath)
		{
			const string shaderStart = @"  m_Shader: ";
			const int guidLength = Utils.GuidLength;
			const string guidStart = "guid: ";
			var guidStartLength = guidStart.Length;

			foreach (var line in File.ReadAllLines(assetPath))
			{
				if (!line.StartsWith(shaderStart, StringComparison.InvariantCulture))
					continue;

				var startIndex = line.IndexOf(guidStart, StringComparison.InvariantCulture);

				if (startIndex == -1 || startIndex + guidStartLength + guidLength >= line.Length)
					continue;

				var guid = line.Substring(startIndex + guidStartLength, guidLength);
				if (Utils.IsGuid(guid))
					return guid;
			}

			return string.Empty;
		}

		private static string GetSelectedFolderPath()
		{
			foreach (var obj in Selection.GetFiltered<Object>(SelectionMode.Assets))
			{
				var path = AssetDatabase.GetAssetPath(obj);
				if (string.IsNullOrEmpty(path))
					continue;

				if (Directory.Exists(path))
					return path;
			}

			return string.Empty;
		}
	}
}
