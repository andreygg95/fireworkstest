/*
 * Use this script if missing shader references in one project can be restored from another.
 *
 * 1. Add this script to the project where shader references are correct.
 *
 * 2. Add .txt file with GUIDs to search for to the project (each line represents one GUID;
 *    create file manually, or use SearchForMissingShaders script).
 *
 * 3. Select .txt file in the Project window, and run this menu item. It will log
 *    search process into Console window, and write all found shader names to
 *    the output file and place it under the same directory as the source file.
 *    Each line is going to consist of GUID and corresponding shader name in quotes,
 *    separated by space:
 *    guid1 "shader1_name"
 *    ...
 *    guidN "shaderN_name"
 */

using System;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Bini.ToolKit.SvgShaders.Editor
{
	// TODO: This is a temporary solution to make sure SVG shader guids are the same in all projects - see TK-282.
	// As soon as the problem is eliminated, script should either be removed,
	// or reworked to perform wider range of tasks.
	public class SearchForShadersByGuid
	{
		private const string MenuItemName = "Search For GUIDs From File";
		private const string OutputFileName = "GUIDShaderNames.txt";

		[MenuItem(MenuItemUI.Group + MenuItemName, priority = 3)]
		private static void Run()
		{
			var menuItemUI = new MenuItemUI(MenuItemName);

			var selectionPath = Utils.GetSelectedTxtFilePath();
			if (string.IsNullOrEmpty(selectionPath))
			{
				menuItemUI.ShowErrorDialog("First select .txt file " +
					"containing shader GUIDs in the Project window!");
				return;
			}

			if (!PerformSearch(selectionPath, menuItemUI, out var searchOutput))
				return;

			var outputFilePath = Path.GetDirectoryName(selectionPath)
			                     + Path.DirectorySeparatorChar + OutputFileName;

			File.WriteAllText(outputFilePath, searchOutput);
			AssetDatabase.Refresh();

			menuItemUI.ShowFinishDialog($"All found shader names are saved to {outputFilePath}");
		}

		private static bool PerformSearch(string selectionPath, MenuItemUI menuItemUI, out string output)
		{
			Debug.Log($"Reading content of selected file: {selectionPath}");

			var fileContent = File.ReadAllLines(selectionPath);
			if (fileContent.Length == 0)
			{
				menuItemUI.ShowErrorDialog("Selected .txt file is empty!");
				output = string.Empty;
				return false;
			}

			if (!menuItemUI.ShowStartDialog("We're going to search for " +
				$"{fileContent.Length} shader{Utils.GetPluralPrefix(fileContent)} listed in file \n{selectionPath}"))
			{
				output = string.Empty;
				return false;
			}

			var result = new StringBuilder();

			foreach (var line in fileContent)
			{
				var shaderName = GetShaderName(line);
				if (!string.IsNullOrEmpty(shaderName))
					result.AppendLine($"{line} \"{shaderName}\"");
			}

			if (result.Length == 0)
			{
				menuItemUI.ShowFinishDialog("No shaders with given GUIDs were found!");
				output = string.Empty;
				return false;
			}

			output = result.ToString();
			return true;
		}

		private static string GetShaderName(string guid)
		{
			if (!Utils.IsGuid(guid))
			{
				Debug.LogError($"File line is not a GUID:\n{guid}");
				return string.Empty;
			}

			var assetPath = AssetDatabase.GUIDToAssetPath(guid);
			if (string.IsNullOrEmpty(assetPath))
			{
				Debug.LogWarning($"No asset with GUID {guid} was found");
				return string.Empty;
			}

			var assetType = AssetDatabase.GetMainAssetTypeAtPath(assetPath);
			if (assetType != typeof(Shader))
			{
				Debug.LogWarning($"GUID {guid} is taken by a non-shader asset!");
				return string.Empty;
			}

			try
			{
				var shaderNameLine = File.ReadLines(assetPath).First();
				var firstIndex = shaderNameLine.IndexOf("\"", StringComparison.InvariantCulture) + 1;
				var lastIndex = shaderNameLine.LastIndexOf("\"", StringComparison.InvariantCulture);

				var shaderName = shaderNameLine.Substring(firstIndex, lastIndex - firstIndex);
				Debug.Log($"Found shader with GUID {guid}: {shaderName}");
				return shaderName;
			}
			catch (Exception e)
			{
				Debug.LogError($"Can not retrieve shader name from asset with GUID {guid}! Error: {e.Message}");
				return string.Empty;
			}
		}
	}
}
