﻿/*
 * This script populates 'from' and 'to' fields in Guid Rewriter window
 * based on the content of the given .txt file.
 *
 * Input file should contain GUID and corresponding shader name in each line:
 *    guid1 "shader1_name"
 *    ...
 *    guidN "shaderN_name"
 * You can create file manually, or use SearchForShadersByGuid script.
 *
 * Script execution is also logged into Console window.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Bini.ToolKit.SvgShaders.Editor
{
	// TODO: This is a temporary solution to make sure SVG shader guids are the same in all projects - see TK-282.
	// As soon as the problem is eliminated, script should either be removed,
	// or reworked to perform wider range of tasks.
	public class GuidRewriterFromFile
	{
		private const string MenuItemName = "Update GUID Rewriter From File";

		[MenuItem(MenuItemUI.Group + MenuItemName, priority = 4)]
		private static void Run()
		{
			var menuItemUI = new MenuItemUI(MenuItemName);

			var selectionPath = Utils.GetSelectedTxtFilePath();
			if (string.IsNullOrEmpty(selectionPath))
			{
				menuItemUI.ShowErrorDialog("First select .txt file " +
					"containing GUIDs and shader names in the Project window!");
				return;
			}

			ProcessFileContent(selectionPath, menuItemUI);
		}

		private static void ProcessFileContent(string selectionPath, MenuItemUI menuItemUI)
		{
			Debug.Log($"Reading content of selected file: {selectionPath}");

			var fileContent = File.ReadAllLines(selectionPath);
			if (fileContent.Length == 0)
			{
				menuItemUI.ShowErrorDialog("Selected .txt file is empty!");
				return;
			}

			if (!menuItemUI.ShowStartDialog($"We're going to process GUIDs listed in file \n{selectionPath}"))
				return;

			var guids = new List<(string, string)>();

			foreach (var line in fileContent)
			{
				var result = ParseFileLine(line);
				if (result == null)
					continue;

				guids.Add(result.Value);
			}

			if (guids.Count == 0)
			{
				menuItemUI.ShowFinishDialog("No GUIDs to rewrite were found!");
				return;
			}

			GuidRewriter.Init();
			GuidRewriter.Window.from = guids.Select(x => x.Item1).ToArray();
			GuidRewriter.Window.to = guids.Select(x => x.Item2).ToArray();
			menuItemUI.ShowFinishDialog("Updated 'from' and 'to' fields in Guid Rewriter window.");
		}

		private static (string GuidFrom, string GuidTo)? ParseFileLine(string line)
		{
			var firstIndex = line.IndexOf("\"", StringComparison.InvariantCulture);
			var lastIndex = line.LastIndexOf("\"", StringComparison.InvariantCulture);
			if (firstIndex == -1 || lastIndex - firstIndex == 0)
			{
				Debug.LogError($"Shader name is supposed to be in quotes:\n{line}");
				return null;
			}

			var guidFrom = line.Substring(0, firstIndex - 1).Trim();
			if (!Utils.IsGuid(guidFrom))
			{
				Debug.LogError($"File line does not contain a GUID:\n{line}");
				return null;
			}

			var shaderName = line.Substring(firstIndex + 1, lastIndex - firstIndex - 1);
			var shader = Shader.Find(shaderName);
			if (shader == null)
			{
				Debug.LogError($"No shader named '{shaderName}' is found in the project!");
				return null;
			}

			return (guidFrom, AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(shader)));
		}
	}
}
